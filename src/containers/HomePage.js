import React, {Component} from 'react';
import InputBox from "../components/InputBox/Inputbox";
import HistoryTable from "../components/HistoryTable/HistoryTable"

class Home extends Component {
    constructor(props){
        super(props);
        this.state = {
            verifyResult: [], 
            resultReceived: []
        };
        this.handleVerifyResult = this.handleVerifyResult.bind(this)
    }

    handleVerifyResult(value){
        const newState = this.state.verifyResult
        newState.push(value)
        this.setState({verifyResult: newState, resultReceived: value})
    }
    
    render(){
      return (
        <div>
            <div className="homepage-container">
                <h1>Phone Verification</h1>
                <InputBox handleVerifyResult={this.handleVerifyResult}/>
                <div className="result-box-container">
                    <h2 className="result-box-header">Verifiction Result</h2>
                    <form className="result-box-form">
                        <div>
                            <p>Phone Number: {this.state.resultReceived.number}</p>
                            <p>Country: {this.state.resultReceived.country_name}</p>
                            <p>Country Prefix: {this.state.resultReceived.country_prefix}</p>
                            <p>Carrier: {this.state.resultReceived.carrier}</p>
                            <p>Validity: {this.state.resultReceived.valid === true? 'True': this.state.resultReceived.valid === false?'False': ''}</p>
                            <p>International Format: {this.state.resultReceived.international_format}</p>
                            <p>Local Format: {this.state.resultReceived.local_format}</p>
                            <p>Submit Time: {this.state.resultReceived.submit_time}</p>
                        </div>
                    </form>
                </div>
                <HistoryTable verifyResult={this.state.verifyResult}/>
            </div>
        </div>
      )
    }
  }
  export default Home;
  