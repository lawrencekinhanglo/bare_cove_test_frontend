const axios = require('axios')
var config = require('../../logics/config')

export function phoneVerification(req){
    var payload = req
    console.log(`${config.baseURL}/backend/v1/phone-verification/`,)
    return axios.post(`${config.baseURL}/backend/v1/phone-verification/`, payload).then((res)=>{
        return res.data
    })
}