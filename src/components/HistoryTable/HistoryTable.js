import React, {Component} from 'react';
import ReactTable from "react-table";
import moment from 'moment'

class HistoryTable extends Component {
    constructor(props){
        super(props);
        this.state = {
            selectedValue: ''
        }
    }

    render(){
        const columns = [
            {   
                Header: 'Phone Number', 
                accessor: 'number'
            },  {   
                Header: 'Country', 
                accessor: 'country_code'
            },  {   
                Header: 'Country Prefix', 
                accessor: 'country_prefix'
            } , {   
                Header: 'International Format', 
                accessor: 'international_format'
            }  ,{   
                Header: 'Local Format', 
                accessor: 'local_format'
            }  ,{   
                Header: 'Location', 
                accessor: 'location'
            } , {   
                Header: 'Carrier', 
                accessor: 'carrier'
            } , {   
                Header: 'Validity', 
                accessor: 'valid',
                Cell: row => 
                (<span>{row.value === false? 'Invalid': 'Valid' }</span>)
            }  ,{   
                Header: 'Submit Time', 
                accessor: 'submit_time',
                Cell: row => 
                    (<span>{moment(row.value).format('YYYY MM DD hh:mm:ss')}</span>)
            }]
        console.log(this.props.verifyResult, 'history')

        return (
            <div className="history-table-container">
                <ReactTable data={this.props.verifyResult} filterable columns={columns}/>
            </div>
        )
    }
}
export default HistoryTable