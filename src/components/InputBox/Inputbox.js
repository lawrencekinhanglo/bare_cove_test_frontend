import React, {Component} from 'react';
import Selector from '../Selector/Selector'
import {phoneVerification} from '../../api/phoneVerify/index'
class InputBox extends Component {
    constructor(props){
        super(props);
        this.state = {
            inputValue: '',
            selectedCountry: '',
        }

        this.handleInputChangeCountry = this.handleInputChangeCountry.bind(this)
        this.handleInputChangeNumber = this.handleInputChangeNumber.bind(this)
        this.handleSubmitNumber = this.handleSubmitNumber.bind(this)
    }

    handleInputChangeCountry(value){
        this.setState({selectedCountry: value})
    }

    handleInputChangeNumber(event){
        this.setState({inputValue: event.target.value})
    }

    handleSubmitNumber(){
        phoneVerification({number: this.state.inputValue, country_code: this.state.selectedCountry}).then((response) => {
            if(response.status){
                this.props.handleVerifyResult(response.data)
            }
        }).catch((error)=>
            console.log('Error: ', error)
        )
    }

    render(){
        return (
            <div className="input-box-container">
                <Selector selectedCountry={this.handleInputChangeCountry}/>
                <input type="text" className="verify-input-box" onChange={this.handleInputChangeNumber}/>
                <button className="verify-submit-button" onClick={this.handleSubmitNumber}>Validate</button>
            </div>
        )
    }
}
export default InputBox