import React, {Component} from 'react';
import './App.css';
import {BrowserRouter, Route} from 'react-router-dom'
import Home from "./containers/HomePage";


class App extends Component {
  constructor(props){
    super(props);
      this.state = {verifyResult: ''};
  }
  componentDidMount() {
    document.title = 'Phone Verification';
  }

  render(){
    return (
      <div>
        <BrowserRouter>
          <div className="App">
            <div className="page-container">
              <Home/>
            </div>
          </div>
        </BrowserRouter>
      </div>
    )
  }
}
export default App;
